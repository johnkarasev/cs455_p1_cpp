//
// Created by John Karasev on 5/20/18.
//

#ifndef CS445_P1_IACTIVATION_H
#define CS445_P1_IACTIVATION_H


class IActivation {
public:
    IActivation() = default;
    ~IActivation() = default;
    virtual double Fire(double) = 0;
    virtual double Derivative(double) = 0;
};


#endif //CS445_P1_IACTIVATION_H
