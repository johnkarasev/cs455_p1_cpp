//
// Created by John Karasev
//

#ifndef CS445_P1_IWEIGHTUPDATE_H
#define CS445_P1_IWEIGHTUPDATE_H
/*weight update that perceptron uses, needs to be overidden.*/
class IWeightUpdate {
public:
    virtual void update(double *weight, double *delta_weight, double error, double input) = 0;
};
#endif //CS445_P1_IWEIGHTUPDATE_H
