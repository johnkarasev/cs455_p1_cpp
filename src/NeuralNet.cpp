//
// Created by John Karasev
//
#include <gsl/gsl_blas.h>
#include "NeuralNet.h"
#include "Perceptron.h"
#include <cstdlib>
#include <cstdio>

NeuralNet::NeuralNet(Perceptron *start, Perceptron *end, gsl_vector *input) {
    this->start = start;
    this->end = end;
    this->input = input;
}

NeuralNet::~NeuralNet() { //delete all perceptrons.
    Perceptron *next = this->start->next_layer;
    while(true) {
        delete next->prev_layer;
        if(next->is_end) {
            delete next;
            break;
        }
        next = next->next_layer;
    }
}

//for each layer fire the perceptrons.
void NeuralNet::feedForward(gsl_vector *input) {
    gsl_vector_view view = gsl_vector_subvector(this->input, 0, input->size); //to not overwrite bias.
    gsl_blas_dcopy(input, &view.vector);
    Perceptron* temp = this->start;
    for ( ; !temp->is_end; temp = temp->next_layer )
        temp->Fire();
    temp->Fire();
}

//caclulate error terms then update weights.
void NeuralNet::backPropagate(gsl_vector *target) {
    Perceptron* temp = this->end;
    for ( ; !temp->is_start; temp = temp->prev_layer )
        temp->calcErrorTerms(target);
    temp->calcErrorTerms(target);
    for ( ; !temp->is_end; temp = temp->next_layer )
        temp->updateWeights();
    temp->updateWeights();
}

//given a NeuralNetConfig, build a nn and return it.
NeuralNet* NeuralNet::buildNN(NeuralNetConfig *config, int layers, double min, double max) {
    int i = layers - 1;
    Perceptron *end;
    Perceptron *layer = end = new Perceptron((size_t)config[i].input, (size_t)config[i].output,
                                             config[i].activation, config[i].update);
    layer->initWeights(min, max);
    for ( int j = layers - 2; j >= 0; j--) {
        bool start = false;
        if (!j) start = true;
        layer->prev_layer = new Perceptron((size_t)config[j].input, (size_t)config[j].output, config[j].bias, layer->input,
                                           config[j].activation, config[j].update, start);
        layer->prev_layer->next_layer = layer;
        layer->prev_layer->initWeights(min, max);
        layer = layer->prev_layer;
    }
    auto nn = new NeuralNet(layer, end, layer->input);
    return nn;
}

//appy a thresholds on the ouptut to round off to 1.0 or 0.0
void NeuralNet::applyThresholds(double min, double max) {
    for ( size_t i = 0; i < this->end->output->size; i++) {
        double* o = gsl_vector_ptr(this->end->output, i);
        if (*o >= max) *o = 1.0;
        else if (*o <= min) *o = 0.0;
    }
}