//
// Created by John Karasev
//

#ifndef CS445_P1_NEURALNET_H
#define CS445_P1_NEURALNET_H


#include "Perceptron.h"

/*pass an array of this structs into nnbuilder and it will build and return the nn*/
struct NeuralNetConfig {
    double bias;
    int input;
    int output;
    IActivation *activation;
    IWeightUpdate *update;
};
//see .cpp for descriptions.
class NeuralNet {
public:
    gsl_vector* input;
    Perceptron* start;
    Perceptron* end;
    NeuralNet(Perceptron* start, Perceptron* end, gsl_vector* input_view);
    ~NeuralNet();
    static NeuralNet* buildNN(NeuralNetConfig *config, int layers, double min, double max);
    void feedForward(gsl_vector* input);
    void backPropagate(gsl_vector* target);
    void applyThresholds(double min, double max);
};


#endif //CS445_P1_NEURALNET_H
