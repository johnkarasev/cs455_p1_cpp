//
// Created by John Karasev
//

#include "Perceptron.h"
#include "IActivation.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <random>
#include <cstdlib>
#include <cmath>

//print a vector.
void print_vec(gsl_vector* vec) {
    if ( vec->size == 784 ) {
        for ( int i = 0; i < vec->size; i++) {
            if ( ! ( i % 28)) printf("\n");
            double val = gsl_vector_get(vec, i);
            if ( val > 0.0) printf("1.1 ");
            else printf("0.0 ");
        }
        printf("\n");
        return;
    }
    for ( int i = 0; i < vec->size; i++) {
        printf("%f ", gsl_vector_get(vec, i));
    }
    printf("\n");
}

//used for hidden and input layers.
Perceptron::Perceptron(size_t input_size, size_t output_size, double bias, gsl_vector *output, IActivation* activation, IWeightUpdate* update, bool is_start) {
    this->input = gsl_vector_alloc(input_size);
    this->dot_product = gsl_vector_alloc(output_size);
    this->activation = activation;
    this->update = update;
    this->output = output;
    this->error_terms = gsl_vector_alloc(output_size);
    this->weights = gsl_matrix_alloc(output_size, input_size);
    this->delta_weights = gsl_matrix_calloc(output_size, input_size);
    this->is_end = false;
    this->is_start = is_start;
    gsl_vector_set(this->input, input_size - 1, bias );
}

//used for ouput layer.
Perceptron::Perceptron(size_t input_size, size_t output_size, IActivation *activation, IWeightUpdate* update) {
    this->input = gsl_vector_alloc(input_size);
    this->dot_product = gsl_vector_alloc(output_size);
    this->output = gsl_vector_alloc(output_size);
    this->error_terms = gsl_vector_alloc(output_size);
    this->weights = gsl_matrix_alloc(output_size, input_size);
    this->delta_weights = gsl_matrix_calloc(output_size, input_size);
    this->activation = activation;
    this->update = update;
    this->is_start = false;
    this->is_end = true;
}

Perceptron::~Perceptron() {
    gsl_matrix_free(this->weights);
    gsl_matrix_free(this->delta_weights);
    gsl_vector_free(this->input);
    gsl_vector_free(this->dot_product);
    if(this->is_end) gsl_vector_free(this->output);
}

//initialize wieghts to range of min to max.
void Perceptron::initWeights(double min, double max) {
    std::random_device random_device; // create object for seeding
    std::mt19937 engine{random_device()}; // create engine and seed it
    std::uniform_real_distribution<double> dist(min, max);
    for ( size_t i = 0; i < this->weights->size1; i++ ) {
        for ( size_t j = 0; j < this->weights->size2; j++) {
            gsl_matrix_set(this->weights, i, j, dist(engine) );
        }
    }
}

//fire a perceptron matrix vector mul inputs with weights, then activate results using IActivation object type.
void Perceptron::Fire() {
    gsl_blas_dgemv(CblasNoTrans, 1.0, this->weights, this->input, 0.0, this->dot_product);
    gsl_vector_view view = gsl_vector_subvector(this->output, 0, this->dot_product->size);
    for ( size_t i = 0; i < this->dot_product->size; i++) {
        double dp = gsl_vector_get(this->dot_product, i);
        gsl_vector_set(&view.vector, i, this->activation->Fire(dp));
    }
}

//after error terms are updated, updated weights using the IWeightUpdate type object provided.
void Perceptron::updateWeights() {
    for ( size_t i = 0; i < this->weights->size1; i++ ) {
        for ( size_t j = 0; j < this->weights->size2; j++ ) {
            double *weight = gsl_matrix_ptr(this->weights, i, j);
            double *delta_weight = gsl_matrix_ptr(this->delta_weights, i, j);
            double error = gsl_vector_get(this->error_terms, i);
            double input = gsl_vector_get(this->input, j);
            this->update->update(weight, delta_weight, error, input);
        }
    }
}

//calculate error terms using the activation object's derivative function.
void Perceptron::calcErrorTerms(gsl_vector* labels) {
    if ( this->is_end) {
        for ( size_t i = 0; i < this->output->size; i++) {
            double dp = gsl_vector_get(this->dot_product, i);
            double target = gsl_vector_get(labels, i);
            double predicted = gsl_vector_get(this->output, i);
            double error = this->activation->Derivative(dp) * ( target - predicted);
            gsl_vector_set(this->error_terms, i, error);
        }
        return;
    }
    for ( size_t j = 0; j < this->error_terms->size; j++) {
        double wd = 0.0;
        double hp = this->activation->Derivative(gsl_vector_get(this->dot_product, j));
        for ( size_t k = 0; k < this->next_layer->weights->size1; k++ )
            wd += gsl_matrix_get(this->next_layer->weights, k, j) * gsl_vector_get(this->next_layer->error_terms, k);
        gsl_vector_set(this->error_terms, j, wd * hp );
    }
}




