//
// Created by John Karasev on 5/20/18.
//

#ifndef CS445_P1_PERCEPTRON_H
#define CS445_P1_PERCEPTRON_H

#include <gsl/gsl_matrix.h>
#include "IActivation.h"
#include "IWeightUpdate.h"

//see cpp file for comments.

void print_vec(gsl_vector* vec);

class Perceptron {
public:
    gsl_vector* input;
    gsl_vector* dot_product;
    gsl_vector* output;
    gsl_matrix* weights;
    gsl_matrix* delta_weights;
    gsl_vector* error_terms;
    IActivation* activation;
    IWeightUpdate* update;
    Perceptron* prev_layer;
    Perceptron* next_layer;
    bool is_start;
    bool is_end;
    Perceptron(size_t input_size, size_t output_size, double bias, gsl_vector* next_input,
               IActivation* activation, IWeightUpdate *update, bool is_start);
    Perceptron(size_t input_size, size_t output_size, IActivation* activation,
               IWeightUpdate *update);
    ~Perceptron();
    void initWeights(double min, double max);
    void Fire();
    void updateWeights();
    void calcErrorTerms(gsl_vector* labels);
};


#endif //CS445_P1_PERCEPTRON_H
