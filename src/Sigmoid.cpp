//
// Created by John Karasev
//

#include "Sigmoid.h"
#include <cmath>

double Sigmoid::Fire(double x) {
    return 1 / ( 1 + exp(-x));
}

double Sigmoid::Derivative(double x) {
    double fired = this->Fire(x);
    return fired * (1 - fired);
}