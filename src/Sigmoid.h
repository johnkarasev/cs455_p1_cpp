//
// Created by John Karasev on 5/20/18.
//

#ifndef CS445_P1_SIGMOID_H
#define CS445_P1_SIGMOID_H


#include "IActivation.h"

class Sigmoid : public IActivation {
public:
    double Fire(double) override;
    double Derivative(double) override;
};


#endif //CS445_P1_SIGMOID_H
