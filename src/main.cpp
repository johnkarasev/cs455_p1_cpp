#include <iostream>
#include "IWeightUpdate.h"
#include "IActivation.h"
#include "Sigmoid.h"
#include "NeuralNet.h"
#include <fstream>      // fstream
#include <vector>
#include <string>
#include <algorithm>    // copy
#include <iterator>     // ostream_operator
#include <boost/tokenizer.hpp>
#include <gsl/gsl_matrix.h>


//a child of weightupdate that is used in the perceptron class to update weights.

class SingleBatch : public IWeightUpdate {
public:
    double learning_rate;
    double momentum_term;
    SingleBatch(double learning_rate, double momentum_term) {
        this->learning_rate = learning_rate;
        this->momentum_term = momentum_term;
    }
    void changeMomentum(double momentum_term) {
        this->momentum_term = momentum_term;
    }
    void changeLearningRate(double learning_rate) {
        this->learning_rate = learning_rate;
    }
    //update function.
    void update(double *weight, double* weight_delta, double error, double input) override {
        *weight_delta = this->learning_rate * error * input + this->momentum_term * (*weight_delta);
        *weight = (*weight) + (*weight_delta);
    }

};

//confusion matrix
class Confusion {
public:
    gsl_matrix* matrix;
    Confusion(int rows, int columns) {
        this->matrix = gsl_matrix_calloc((size_t)rows, (size_t)columns);
    }
    void reset() { //set all entries to 0
        gsl_matrix_set_all(this->matrix, 0.0);
    }
    //update the matrix.
    void update(gsl_vector* predicted, gsl_vector* target) {
        size_t correct = gsl_vector_max_index(target);
        size_t predict = gsl_vector_max_index(predicted);
        double *count = gsl_matrix_ptr(this->matrix, correct, predict);
        *count += 1.0;
    }
    void print() {
        for ( size_t i = 0; i < this->matrix->size1; i++){
            for ( size_t j = 0; j < this->matrix->size2; j++)  {
                if ( j == 0 ) printf("\n");
                printf("%f,",gsl_matrix_get(this->matrix, i, j) );
            }
        }
        printf("\n");
    }
    //compute accuracy
    double get_accuracy() {
        double total = 0.0, correct = 0.0;
        for (size_t i = 0; i < matrix->size1; i++) {
            for (size_t j = 0; j < matrix->size2; j++) {
                double count = gsl_matrix_get(this->matrix, i, j);
                if (i == j) correct += count;
                total += count;
            }
        }
        return correct / total;
    }
};

//takes path and reads the data into array of gsl_vectors.
void readmnist(std::string trainpath, gsl_vector** inputs, gsl_vector** targets) {
    using namespace std;
    using namespace boost;
    ifstream in(trainpath.c_str());
    if (!in.is_open()) exit(1);
    typedef tokenizer< escaped_list_separator<char> > Tokenizer;
    vector< string > vec;
    string line;
    int linecount = 0;
    while (getline(in,line)) {
        //if ( linecount > 10001) break;
        Tokenizer tok(line);
        vec.assign(tok.begin(),tok.end());
        gsl_vector* target = gsl_vector_calloc(10);
        gsl_vector_set(target, (size_t) atoi(vec[0].c_str()), 1.0);
        targets[linecount] = target;
        gsl_vector* input = gsl_vector_alloc(784);
        for ( int i = 1 ; i < vec.size(); i++)
            gsl_vector_set(input, (size_t)i - 1, atof(vec[i].c_str()) / 255.0);
        inputs[linecount] = input;
        linecount++;
    }
}

// applies one epoch of learning, prints results in comma dilemted form.

void epoch(NeuralNet *nn, gsl_vector** train_inputs, gsl_vector** train_target,
           gsl_vector** test_inputs, gsl_vector** test_target, Confusion* ctr, Confusion* cte, int epoch, int train_num) {
    ctr->reset();
    cte->reset();
    if(epoch) { // if first epoch, do not train.
        for ( int j = 0; j < train_num; j++) {
            nn->feedForward(train_inputs[j]);
            nn->backPropagate(train_target[j]);
            nn->applyThresholds(0.1, 0.9);
        }
    }
    // test the
    for ( int j = 0; j < train_num; j++) {
        nn->feedForward(train_inputs[j]);
        nn->applyThresholds(0.1, 0.9);
        ctr->update(nn->end->output, train_target[j]);
    }
    for ( int j = 0; j < 10000; j++) {
        nn->feedForward(test_inputs[j]);
        nn->applyThresholds(0.1, 0.9);
        cte->update(nn->end->output, test_target[j]);
    }
    for ( int j = train_num; j < 60000; j++) {
        nn->feedForward(train_inputs[j]);
        nn->applyThresholds(0.1, 0.9);
        cte->update(nn->end->output, train_target[j]);
    }
    printf("%d,%f,%f\n", epoch, ctr->get_accuracy(), cte->get_accuracy());
}



int main() {
    gsl_vector* train_inputs[60000];
    gsl_vector* train_target[60000];

    gsl_vector* test_inputs[10000];
    gsl_vector* test_target[10000];

    auto update = new SingleBatch(0.1, 0.9);
    IActivation* activation = new Sigmoid();
    auto ctr = new Confusion( 10, 10 );
    auto cte = new Confusion( 10, 10 );

    struct NeuralNetConfig config[10];

    for ( int i = 0; i < 10; i++) {
        config[i].bias = 1.0;
        config[i].update = update;
        config[i].activation = activation;
    }

    //configure the layers of the network.
    config[0].input = 785;
    config[0].output = 99;
    config[1].input = 100;
    config[1].output = 10;

    //read the mnist dataset. //MAY NEED TO CHANGE PATHS TO ABSOLUTE PATHS
    printf("Reading Train Mnist into main memory...\n");
    readmnist("../mnist/mnist_train.csv", train_inputs, train_target);
    printf("sucessfully read..\n");
    printf("Reading Test Mnist into main memory...\n");
    readmnist("../mnist/mnist_test.csv", test_inputs, test_target);
    printf("sucessfully read..\n");

/*
    //train and test for different sizes of hidden layer.
    for ( int n : {20, 50, 100}) {
        printf("n=%d\n", n);
        config[0].output = n - 1;
        config[1].input = n;
        NeuralNet* nn = NeuralNet::buildNN(config, 2, -0.05, 0.05);
        printf("epoch,train,test\n");
        for ( int i = 0; i < 51; i++ )
            epoch(nn, train_inputs, train_target, test_inputs, test_target, ctr, cte, i, 60000);
        delete nn;
        printf("\n");
        ctr->print();
        printf("\n");
        cte->print();
        printf("\n\n");
    }


    //train and test with different momentum values.
    for ( double alpha : {0.0, 0.25, 0.5}) {
        NeuralNet* nn = NeuralNet::buildNN(config, 2, -0.05, 0.05);
        printf("alpha=%f\n", alpha);
        update->changeMomentum(alpha);
        printf("epoch,train,test\n");
        for ( int i = 0; i < 51; i++ )
            epoch(nn, train_inputs, train_target, test_inputs, test_target, ctr, cte, i, 60000);
        printf("\n\n");
        delete nn;
        printf("\n");
        ctr->print();
        printf("\n");
        cte->print();
        printf("\n\n");
    }

*/
    //train and test for different train set sizes.
    update->changeMomentum(0.9);
    for ( int size : {30000}) {
        NeuralNet* nn = NeuralNet::buildNN(config, 2, -0.05, 0.05);
        printf("size=%d\n", size);
        printf("epoch,train,test\n");
        for ( int i = 0; i < 51; i++ )
            epoch(nn, train_inputs, train_target, test_inputs, test_target, ctr, cte, i, size);
        printf("\n\n");
        delete nn;
        printf("\n");
        ctr->print();
        printf("\n");
        cte->print();
        printf("\n\n");
    }
    std::cout <<"Finished Successfully." << std::endl;



    return 0;
}


/*
 *
 *
 *
 *  for ( int j = 0; j < 60000; j++) {
        nn->feedForward(train_inputs[j]);
        nn->backPropagate(train_target[j]);
        nn->applyThresholds(0.1, 0.9);
    }
 *      printf("INPUT   : ");
        print_vec(train_inputs[j]);
        printf("TARGET  : ");
        print_vec(train_target[j]);
        printf("PREDICT : ");
        print_vec(nn->end->output);
        printf("ERROR   : ");
        print_vec(nn->end->error_terms);
        printf("\n");


    using namespace std;
    using namespace boost;
    string data("../mnist/mnist_test.csv");

    ifstream in(data.c_str());
    if (!in.is_open()) return 1;

    typedef tokenizer< escaped_list_separator<char> > Tokenizer;
    vector< string > vec;
    string line;

    while (getline(in,line))
    {
        Tokenizer tok(line);
        vec.assign(tok.begin(),tok.end());

        // vector now contains strings from one row, output to cout here
        copy(vec.begin(), vec.end(), ostream_iterator<string>(cout, "|"));

        cout << "\n----------------------" << endl;
    }

    config[0].bias = config[1].bias = 1.0;
    config[0].activation = config[1].activation = activation;
    config[0].update = config[1].update = update;
    config[0].input = 4;
    config[0].output = 2;
    config[1].input = 3;
    config[1].output = 1;

    NeuralNet* nn = NeuralNet::buildNN(config, 2, -0.05, 0.05);
    gsl_vector* inputvec[8] = { gsl_vector_alloc(3), gsl_vector_alloc(3), gsl_vector_alloc(3), gsl_vector_alloc(3),
                              gsl_vector_alloc(3), gsl_vector_alloc(3), gsl_vector_alloc(3), gsl_vector_alloc(3)};
    gsl_vector *outputvec[8] = { gsl_vector_alloc(1), gsl_vector_alloc(1), gsl_vector_alloc(1), gsl_vector_alloc(1),
                                gsl_vector_alloc(1), gsl_vector_alloc(1), gsl_vector_alloc(1), gsl_vector_alloc(1)};
    double inputs[8][3] = {{0.0,0.0,0.0},
                           {0.0,0.0,1.0},
                           {0.0,1.0,0.0},
                           {0.0,1.0,1.0},
                           {1.0,0.0,0.0},
                           {1.0,0.0,1.0},
                           {1.0,1.0,0.0},
                           {1.0,1.0,1.0}};

    for ( int i = 0; i < 8; i++ ){
        for ( int j = 0; j < 3; j++ ) {
            gsl_vector_set(inputvec[i], (size_t)j, inputs[i][j]);
        }
        gsl_vector_set(outputvec[i], 0, (double)(i) / 8.0);
    }

    for ( int i = 0; i < 500; i++ ) {
        for ( int j = 0; j < 8 ; j++) {
            nn->feedForward(inputvec[j]);
            nn->backPropagate(outputvec[j]);
            nn->applyThresholds(0.1, 0.9);
            printf("predict %f target: %f\n\n", gsl_vector_get(nn->end->output, 0), gsl_vector_get(outputvec[j], 0));
        }
    }
*/